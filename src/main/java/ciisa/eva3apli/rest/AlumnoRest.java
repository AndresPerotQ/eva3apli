/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciisa.eva3apli.rest;

import ciisa.eva3apli.dao.AlumnoJpaController;
import ciisa.eva3apli.dao.exceptions.NonexistentEntityException;
import ciisa.eva3apli.entity.Alumno;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author andre
 */
@Path ("alumno")
public class AlumnoRest {
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listar(){
    
        AlumnoJpaController dao= new AlumnoJpaController();
        List<Alumno> lista = dao.findAlumnoEntities();
        return Response.ok(200).entity(lista).build();
        
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Alumno alumno){
        AlumnoJpaController dao= new AlumnoJpaController();
        
        try {    
            dao.create(alumno);
        } catch (Exception ex) {
            Logger.getLogger(AlumnoRest.class.getName()).log(Level.SEVERE, null, ex);
        }
         return Response.ok(200).entity(alumno).build();
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizar(Alumno alumno){
        
        AlumnoJpaController dao= new AlumnoJpaController();
        try {
            dao.edit(alumno);
        } catch (Exception ex) {
            Logger.getLogger(AlumnoRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(200).entity(alumno).build();
    }
    
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{rut}")    
    public Response eliminar(@PathParam("rut") String rut){
        
       
        
        try {    
             AlumnoJpaController dao= new AlumnoJpaController();
            dao.destroy(rut);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(AlumnoRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok("persona eliminada").build();
    }
}
